from django.urls import reverse
from django.http import HttpResponse
from .models import Hash
import os
import qrcode
from quipohash.settings import MEDIA_ROOT

from datetime import datetime
from fpdf import FPDF
from django.http import FileResponse, Http404


def get_date_br_and_time(date):
    if 'T' in date:
        spl1 = date.split('T')
    else:
        spl1 = date.split(' ')
    spl2 = spl1[0].split('-')  # spl2 will contain 0:year 1:month 2:day
    if '.' in spl1[1]:  # There will be a '.' when milliseconds are present
        spl3 = spl1[1].split('.')  # spl3[0] will contain the time (hh:mm:ss)
    else:
        spl3 = spl1[1].split('-')  # spl3[0] will contain the time (hh:mm:ss)
    date_br = spl2[2] + '/' + spl2[1] + '/' + spl2[0]
    time = spl3[0]
    return date_br, time


def generate_qrcode_img(folder, qr_file_name, url):
    qr = qrcode.QRCode(
        version=1,
        box_size=10,
        border=5)
    qr.add_data(url)
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')
    img.save(folder + '/' + qr_file_name)


class PDF(FPDF):

    def margin(self):
        self.set_left_margin(margin=30)
        self.set_right_margin(margin=30)
        # self.set_top_margin(margin=1)

    def lines(self):
        self.set_fill_color(255, 223, 0)  # color for inner rectangle
        self.set_draw_color(212, 175, 55)  # metalic golden
        self.set_line_width(1)
        self.set_fill_color(255, 255, 255)  # color for inner rectangle
        self.rect(15.0, 13.0, 181.0, 275.0, 'DF')

    def header(self):
        # Logo
        img = 'assets/img/certification_seal.png'
        self.image(img, -2, 2, 50)
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        self.ln(0)
        self.multi_cell(0, 10, 'E-CERTIFICADO DE AUTENTICIDADE', border=0, align='C')
        self.multi_cell(0, 10, 'ELETRÔNICA DE REGISTRO DIGITAL', border=0, align='C')
        # Line break
        self.ln(1)


def generate_e_certificate(ecert_folder, ecert_file_name, qr_file_name, bchain_record, hash_code, date_br, time):
    now = datetime.now()
    date = now.isoformat()
    date, _ = get_date_br_and_time(str(date))

    qrcode_file = ecert_folder + '/' + qr_file_name

    pdf = PDF()
    pdf.margin()
    pdf.alias_nb_pages()
    pdf.add_page()
    pdf.lines()
    pdf.header()
    pdf.set_font('Times', '', 12)
    pdf.ln(5)
    pdf.multi_cell(0, 6,
                   'A QUIPO TECNOLOGIA LTDA. E-CERTIFICA, para os devidos fins de direito, que o arquivo'
                   ' digital representado pela função hash criptográfica, identificada como SHA-256, através'
                   ' do código:')
    pdf.ln(5)
    pdf.multi_cell(0, 6, hash_code, border=0, align='C')
    pdf.ln(5)
    pdf.multi_cell(0, 6,
                   'foi registrado, via transação blockchain, em conformidade com seus protocolos, podendo'
                   ' a transação ser visualizada em:')
    pdf.ln(5)
    pdf.multi_cell(0, 6, bchain_record, border=0, align='C')
    pdf.ln(5)
    pdf.multi_cell(0, 6, 'A presente autenticação eletrônica do arquivo serve de prova de autenticidade e'
                         ' demonstra a integridade e veracidade de que na data ' +
                   date_br + ' às ' + time + ' a empresa QUIPO Tecnologia Ltda., inscrita no CNPJ sob o número'
                                             ' 36.355.976/0001-24, concluiu o recebimento do'
                                             ' arquivo digital com os'
                                             ' mesmos atributos e as mesmas propriedades que foram reproduzidas na'
                                             ' prova de autenticidade, sendo da empresa Quipo Tecnologia Ltda a'
                                             ' responsabilidade, única e exclusiva, pela geração e armazenamento'
                                             ' em Banco de Dados do código hash, a partir do arquivo'
                                             ' fornecido, e pelo respectivo registro desse código em rede Blockchain.'
                                             ' Importante notar que o arquivo fornecido não é armazenado em Banco de'
                                             ' dados da empresa, sendo imediatamente descartado.')
    pdf.ln(5)
    pdf.multi_cell(0, 6, 'A validade de negócios jurídicos baseados nos registros realizados pela Quipo Tecnologia'
                         ' Ltda., bem como a idoneidade e legalidade (agente capaz; objeto lícito e forma prescrita'
                         ' ou não defesa em lei) do conteúdo do arquivo enviado para geração do hash são de exclusiva'
                         ' responsabilidade do proprietário, remetente ou responsável pela geração do referido'
                         ' conteúdo.')
    pdf.ln(5)
    pdf.multi_cell(0, 6,
                   'Este E-CERTIFICADO foi emitido em ' + date + ' por meio do sistema de autenticação eletrônica da'
                                                                 ' empresa QUIPO Tecnologia Ltda., em estrita'
                                                                 ' conformidade com o Art. 10, § 2º da MP 2200-2/2001,'
                                                                 ' Art. 107 do Código Civil Brasileiro e Art. 411, em'
                                                                 ' seus §§ 2º e 3º do Código de Processo Civil'
                                                                 ' Brasileiro.')
    pdf.ln(5)
    pdf.multi_cell(0, 6,
                   'Consultas adicionais referentes ao referido registro podem ser realizadas diretamente na'
                   ' plataforma web do sistema quipohash, informando o código hash ou o arquivo que deu origem ao'
                   ' mesmo.')
    # footer
    pdf.image('assets/img/quipo_golden_logo.png', 23, 262, 30)
    pdf.image(qrcode_file, 157, 262, 25)

    path_e_cert = ecert_folder + '/' + ecert_file_name
    pdf.output(path_e_cert, 'F')


def e_certificate(request, hash_pk):
    hash_obj = Hash.objects.get(id=hash_pk)

    if not hash_obj.bchain_link:
        return HttpResponse('<h1>This hash was not registered in blockchain yet.</h1>')

    bchain_record = hash_obj.bchain_link
    hash_code = hash_obj.hash
    sent_at = hash_obj.created_at

    date_br, time = get_date_br_and_time(str(sent_at))

    if not bchain_record:
        bchain_record = 'Some error occurred! Contact us.'

    if not hash_code:
        hash_code = 'Some error occurred! Contact us.'

    ecert_file_name = "e_certificate_hash_id_" + str(hash_pk) + ".pdf"
    qr_file_name = 'qrcode_hash_id_' + str(hash_pk) + '.png'
    ecert_folder = MEDIA_ROOT + "/e_certificates"

    # Checking whether basic folders exist and creating them otherwise
    if not os.path.isdir(MEDIA_ROOT):
        os.mkdir(MEDIA_ROOT)

    if not os.path.isdir(ecert_folder):
        os.mkdir(ecert_folder)

    # generating qr_code, if the case
    if not os.path.isfile(ecert_folder + '/' + qr_file_name):
        qr_url = reverse('e_certificate', args=[hash_pk])
        generate_qrcode_img(ecert_folder, qr_file_name, qr_url)

    generate_e_certificate(ecert_folder, ecert_file_name, qr_file_name, bchain_record, hash_code, date_br, time)

    try:
        return FileResponse(open(ecert_folder + '/' + ecert_file_name, 'rb'), content_type='application/pdf')
    except FileNotFoundError:
        raise Http404()
