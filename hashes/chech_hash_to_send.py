import os
from time import sleep
import django
import sys
from scripts.sendTransaction import send_hash
from datetime import date, datetime

#from models import Hash  # Substitua 'your_app' pelo nome do seu aplicativo Django

# Caminho para o diretório do projeto Django
sys.path.append('/opt/bitnami/projetos/quipohash/')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'quipohash.settings')

# Inicializa o Django
django.setup()

from hashes.models import Hash

def send_hash_to_polygon():
    while True:
        # Calculate the datetime two minutes ago from the current time
        hashes = Hash.objects.filter(payment_at__isnull=False, transaction_hash__isnull=True)

        # Verifica se há algum objeto que atenda aos critérios
        if hashes.exists():
            for h in hashes:
                try:
                    # Aqui você teria que definir o método `send_hash`

                    #registra o hash gerado
                    blockchain_address = send_hash(h.hash)
                    h.transaction_hash = blockchain_address

                    #Registra a data e hora da transação
                    today = datetime.today()
                    h.transaction_at = today

                    #adiciona o link
                    h.bchain_link = 'https://polygonscan.com/tx/'+blockchain_address

                    h.save()
                except Exception as e:
                    print(f"An error occurred: {e}")
        # Espera por um intervalo antes de rodar novamente (por exemplo, 2 minutos)
        sleep(120)


if __name__ == '__main__':
    send_hash_to_polygon()