from django.contrib import admin

from .models import Hash, Imagem

admin.site.register(Hash)
admin.site.register(Imagem)
