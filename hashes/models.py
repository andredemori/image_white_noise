from django.db import models
from django.contrib.auth.models import User


class Hash(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)  # will be 'quipo' if no one is logged.
    hash = models.CharField(db_index=True, max_length=64)  # the generated hash
    description = models.CharField(max_length=250)  # a description to facilitate remembering which file is it about.
    price = models.IntegerField(default=0)  # cents
    bchain_link = models.URLField(max_length=250, null=True, blank=True)  # URL pointing to the transaction in the explorer.
    payment_at = models.DateTimeField(null=True, blank=True)  # date/time of payment when it is concluded.
    transaction_at = models.DateTimeField(null=True, blank=True)  # date/time when the transaction was concluded (recorded on the bchain).
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    transaction_hash = models.CharField(max_length=500, null=True, blank=True)
    Picture = models.ImageField(upload_to='media/', null=True, blank=True)  # the generated hash
    Picture_noise = models.ImageField(upload_to='media/', null=True, blank=True)  # the generated hash
    expired = models.BooleanField(null=True, blank=True)

    class Meta:

        constraints = [
            models.UniqueConstraint(fields=['hash'], name='unique_hash')  # ensures that a file is not registered twice
        ]

    def __str__(self):
        return self.hash


class Imagem(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)  # will be 'quipo' if no one is logged.
    Picture = models.ImageField(upload_to='media/', null=True, blank=True)  # the generated hash
    senha = models.CharField(max_length=250)  # a description to facilitate remembering which file is it about.

    class Meta:
        verbose_name = 'Imagem'
        verbose_name_plural = 'Imagens'