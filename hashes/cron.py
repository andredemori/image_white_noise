from datetime import timedelta
from django.utils import timezone
from hashes.models import Hash

def limpar_objetos_antigos():
    # Calculate the datetime two minutes ago from the current time
    limite_temporal = timezone.now() - timedelta(days=365)
    # Filter objects that are older than the calculated datetime
    objetos_antigos = Hash.objects.filter(created_at__lt=limite_temporal,payment_at=None)
    # Delete the filtered objects
    for o in objetos_antigos:
        o.expired = True
        o.save()
