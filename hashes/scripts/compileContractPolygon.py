import json
from web3 import Web3
from solcx import compile_source, compile_files, link_code, install_solc

#Redes PoS necessitam acessar primeiro o middleware / POA chain
from web3.middleware import construct_sign_and_send_raw_middleware
from web3.middleware import geth_poa_middleware

def compile_contract(code):
    compiled_code = compile_files([code], output_values=["abi", "bin"], solc_version="0.4.25")
    print("Code compiled!\n")
    return compiled_code


def deploy_contract(hash_value):
    install_solc('0.4.25')
    #code = "pragma solidity >=0.4.0; contract QuipoContract { uint256 number; function store(uint256 num) public { number = num;} function retrieve() public view returns (uint256){ return number;}}"
    code = "contract.sol"
    compiled_code = compile_contract(code)

    #Test Endpoint: 
    #infura_url = "https://ropsten.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"
    #Main Endpoint:
    #infura_url = "https://mainnet.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"
    #Polygon
    infura_url = "https://polygon-mainnet.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"

    web3 = Web3(Web3.HTTPProvider(infura_url))
    #Inclui o middleware da rede POA
    web3.middleware_onion.inject(geth_poa_middleware, layer=0)

    #My informations (FACE Account Meta Mask Mumbai)
    #myaccount = "0xb1741070FB98D79e8a42e9C4d78DD8bc556008dD"
    #private_key = '99173c3118982c4a4b4a8f4a48f2b616ceefe9bc4dea0df94cd3b04bd0023b85' 
    #Real account bitkeep
    myaccount   = "0xaD98A6e10167dE85a4659e5EE4461ccC27737a83"
    private_key = '' 


    nonce = web3.eth.get_transaction_count(myaccount)

    abi = compiled_code['contract.sol:QuipoContract']['abi']
    bytecode = compiled_code['contract.sol:QuipoContract']['bin']

    #Connect to contract
    contract = web3.eth.contract(abi=abi, bytecode=bytecode)

    print("Start deploy...")

    #senderAccount = web3.eth.account.privateKeyToAccount(private_key)
    senderAccount = web3.eth.account.from_key(private_key)

    #Adiciona middlewatr
    web3.middleware_onion.add(construct_sign_and_send_raw_middleware(senderAccount))
    web3.eth.default_account = senderAccount.address

    transaction = contract.constructor().build_transaction()
    transaction['nonce'] = web3.eth.get_transaction_count(senderAccount.address) # Get correct transaction nonce for sender from the node

    # Sign the transaction
    signed = web3.eth.account.sign_transaction(transaction, senderAccount._private_key)

    # Send it!
    txHash = web3.eth.send_raw_transaction(signed.rawTransaction)

    print(f"Contract  deployed; Waiting to transaction receipt")
    print(f"txHash: " + str(txHash))

    # Wait for the transaction to be mined, and get the transaction receipt
    txReceipt = web3.eth.wait_for_transaction_receipt(txHash)

    print(f"Contract deployed to: {txReceipt.contractAddress}")
    print("Contract Deployed!")

    return txReceipt.contractAddress

deploy_contract(1)

