import json
from web3 import Web3
from solcx import compile_source, compile_files, link_code, install_solc

#Redes PoS necessitam acessar primeiro o middleware / POA chain
from web3.middleware import construct_sign_and_send_raw_middleware
from web3.middleware import geth_poa_middleware

def send_hash(hash_value):
    print("Hash ",hash_value, " received")
    install_solc('0.4.25')

    #Test Endpoint: 
    #infura_url = "https://ropsten.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"
    #Main Endpoint:
    #infura_url = "https://mainnet.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"
    #Polygon
    infura_url = "https://polygon-mainnet.infura.io/v3/8c66ac49df924cb1842afeee0d98a297"

    web3 = Web3(Web3.HTTPProvider(infura_url))
    #Inclui o middleware da rede POA
    web3.middleware_onion.inject(geth_poa_middleware, layer=0)

    #My informations (FACE Account Meta Mask Mumbai)
    #myaccount = "0xb1741070FB98D79e8a42e9C4d78DD8bc556008dD"
    #private_key = '99173c3118982c4a4b4a8f4a48f2b616ceefe9bc4dea0df94cd3b04bd0023b85' 
    #Real account bitkeep
    myaccount   = "0xaD98A6e10167dE85a4659e5EE4461ccC27737a83"
    private_key = ''
    nonce = web3.eth.get_transaction_count(myaccount)

    print("Creating ABI")

    # Contract ABI
    abi = json.loads('[{"constant": true,"inputs": [],"name": "retrieve","outputs": [{"name": "","type": "uint256"}],"payable": false,"stateMutability": "view","type": "function"},{"constant": false,	"inputs": [{"name": "num","type": "uint256"}],"name": "store","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"}]')

    print("Connecting account")

    #Connect to contract
    #Polygon
    address = '0x87063b3F8B5A89980f18A38271Ca60a79aCddA85'


    contract = web3.eth.contract(address=address, abi=abi)

    print("Start transaction...")

    # Set a new value
    tx = contract.functions.store(int(hash_value, 16)).build_transaction({
        'nonce': nonce,
        'maxPriorityFeePerGas': 30000000000,  # 30 Gwei
        'maxFeePerGas': 200000000000  # 200 Gwei
    })
    signed_tx = web3.eth.account.sign_transaction(tx, private_key=private_key)

    print("...")

    # Wait for transaction to be mined
    txHash = web3.eth.send_raw_transaction(signed_tx.rawTransaction)

    print("Contract updated!")

    print(f"transaction sent; Waiting to transaction receipt")
    # Wait for the transaction to be mined, and get the transaction receipt
    txReceipt = web3.eth.wait_for_transaction_receipt(txHash)

    print("Transact deployed to: ", txReceipt.transactionHash.hex())

    return txReceipt.transactionHash.hex()

#Main code
#hash_value = '9610b856924be9cc09f1521c75ba23218eb529e16542590e5bb7310e324b4ed9'
#hash_value = 'bdc367c01206df3a83fc2336be7de2cff2152f3a6c6e15c4b9579d4d4a2dc783' #quipo
#send_hash(hash_value)
