// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.4.25 <0.9.0;

/**
 * @title Quipo Contract
 * @dev Identify phase hash by set command call in contract history
 */
contract QuipoContract {

    uint256 number;

    /**
     * @dev Store value in variable
     * @param num value to store
     */
    function store(uint256 num) public {
        number = num;
    }

    /**
     * @dev Return value 
     * @return value of 'number', last hash sent
     */
    function retrieve() public view returns (uint256){
        return number;
    }
}

