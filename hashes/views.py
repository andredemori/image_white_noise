from django.shortcuts import render
from django.urls import reverse
from hashlib import sha256
from django.contrib.auth.models import User
from .models import Hash

import json
import stripe
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from datetime import date, datetime
from django.views import View

from .tesseract_ocr import TesseractOCR

#from scripts.sendTransaction import send_hash

from asgiref.sync import async_to_sync

import asyncio

stripe.api_key = settings.STRIPE_SECRET_KEY


class SuccessView(TemplateView):
    template_name = "hashes/success.html"

class CancelView(TemplateView):
    template_name = "hashes/query.html"

class CreateCheckoutSessionView(View):
    def post(self, request, *args, **kwargs):
        hash_id = self.kwargs["pk"]
        hash = Hash.objects.get(id=hash_id)
        YOUR_DOMAIN = "https://hash.quipo.io/" #http://127.0.0.1:8000
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[
                {
                    'price_data': {
                        'currency': 'brl',
                        'unit_amount': hash.price,
                        'product_data': {
                            'name': hash.description,
                            # 'images': ['https://i.imgur.com/EHyR2nP.png'],
                        },
                    },
                    'quantity': 1,
                },
            ],
            metadata={
                "product_id": hash.id
            },
            mode='payment',
            success_url = f"{YOUR_DOMAIN}/query_result/"+hash.hash,
            cancel_url= f"{YOUR_DOMAIN}",
        )
        return JsonResponse({
            'id': checkout_session.id
        })


@csrf_exempt
def stripe_webhook(request):
    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, settings.STRIPE_WEBHOOK_SECRET
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']

        customer_email = session["customer_details"]["email"]
        hash_id = session["metadata"]["product_id"]

        hash = Hash.objects.get(id=hash_id)

        today = datetime.today()
        hash.payment_at = today
        hash.save()

        # SEND HASH TO POLYGON
        #loop = asyncio.new_event_loop()
        #asyncio.set_event_loop(loop)
        #try:
            # Sua lógica síncrona aqui
        #    blockchain_address = async_to_sync(send_hash)(hash.hash)
        #    hash.transaction_hash = blockchain_address
        #    hash.save()
        #finally:
        #    loop.close()

        #send_mail(
        #    subject="Here is your product",
        #    message=f"Thanks for your purchase. Here is the product you ordered. The URL is {hash.url}",
        #    recipient_list=[customer_email],
        #    from_email="andre_demori@hotmail.com"
        #)

        # TODO - decide whether you want to send the file or the URL

    elif event["type"] == "payment_intent.succeeded":
        intent = event['data']['object']

        stripe_customer_id = intent["customer"]
        stripe_customer = stripe.Customer.retrieve(stripe_customer_id)

        customer_email = stripe_customer['email']
        product_id = intent["metadata"]["product_id"]

        product = Hash.objects.get(id=product_id)

        #send_mail(
        #    subject="Here is your product",
        #    message=f"Thanks for your purchase. Here is the product you ordered. The URL is ",
        #    recipient_list=[customer_email],
        #    from_email="matt@test.com"
        #)

    return HttpResponse(status=200)

class StripeIntentView(View):
    def post(self, request, *args, **kwargs):
        try:
            req_json = json.loads(request.body)
            customer = stripe.Customer.create(email=req_json['email'])
            hash_id = self.kwargs["pk"]
            hash = Hash.objects.get(id=hash_id)
            intent = stripe.PaymentIntent.create(
                amount=hash.price,
                currency='usd',
                customer=customer['id'],
                metadata={
                    "hash_id": hash.id
                }
            )
            return JsonResponse({
                'clientSecret': intent['client_secret']
            })
        except Exception as e:
            return JsonResponse({ 'error': str(e) })

def get_hash(file):
    """ This function aims to generate a hash (sha256) code for the given file. The file is read in blocks. """

    sha256_hash = sha256()
    # with open(file_name, "rb") as f:
    # Read and update hash string value in blocks of 4K
    for byte_block in iter(lambda: file.read(4096), b""):
        sha256_hash.update(byte_block)
    hash_code = sha256_hash.hexdigest()

    return hash_code


def visualizar(request, hash):
    """View that renders the upload page"""

    aprovado = False

    hash_obj = Hash.objects.get(hash=hash)

    if request.method == 'POST':
        senha = request.POST['senha']

        if hash_obj.description == senha:
            aprovado = True
        else:
            aprovado = False


    context = {
        'hash': hash_obj,
        'aprovado': aprovado
    }
    rendered_page = render(request, 'hashes/senha.html', context)
    return rendered_page

def upload(request):
    """View that renders the upload page"""

    data = {
        'target': reverse('hash'),
        'query_url': reverse('query')
    }
    rendered_page = render(request, 'hashes/upload.html', data)
    return rendered_page


def check_hash_on_db(hash_code):
    # Checking whether this hash is recorded on DB already
    result = Hash.objects.filter(hash=hash_code)
    if result:
        data = {
            'hash_id': result[0].id,
            'hash': result[0].hash,
            'description': result[0].description,
            'price': result[0].price,
            'formatted_price': "R${:,.2f}".format(result[0].price / 100),
            "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY,
            "item": result[0]
        }

        if result[0].bchain_link:
            data['e_certificate_url'] = reverse('e_certificate', args=[result[0].id])
            data['bchain_link'] = result[0].bchain_link
        if result[0].payment_at:
            data['payment_at'] = result[0].payment_at
        if result[0].transaction_at:
            data['transaction_at'] = result[0].transaction_at
        return data

    return None


def generate_hash(request):
    """View that is called when the upload is done and the user submits the file to generate hash.
    Then, this view gets the hash, records initial info on the DB and renders the page that exhibits
    the resulting hash and presents a link for payment"""

    if len(request.FILES) == 0:
        return HttpResponse('<h1>File is missing.</h1>')

    # Generating hash based on the provided file
    gen_hash = get_hash(request.FILES['uploaded_file'])
    picture = request.FILES['uploaded_file']

    data = check_hash_on_db(gen_hash)
    if data:
        rendered_page = render(request, 'hashes/existing_hash.html', data)
        return rendered_page

    # Defining which user will be used to record a new tuple on DB.
    # If there is some user logged in, then use it. Otherwise, utilize user 'quipo'.
    user = request.user
    if not user.id:
        user = User.objects.get(username="quipo")  # getting user object (quipo)
        if not user:
            return HttpResponse('<h1>User quipo is missing.</h1>')

    if not request.POST['file_description']:
        return HttpResponse('<h1>Description is missing.</h1>')

    description = request.POST['file_description']

    sigilo = int(request.POST['sigilo'])

    # Creating a new tuple on table Hash
    hash = Hash.objects.create(user=user,hash=gen_hash,description=description,price=sigilo, Picture=picture)

    new_tuple = {
        'hash_id': hash.id,
        'hash': hash.hash,
        'description': hash.description,
        'price': hash.price,
        'formatted_price': "R${:,.2f}".format(hash.price / 100),
        "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY,
        'item': hash
    }

    rendered_page = render(request, 'hashes/hash.html', new_tuple)
    return rendered_page

def query(request):
    """View that renders the page to make queries (by hash and by the file itself)"""
    data = {
        'target': reverse('query_result'),
        "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY
    }

    #hash = Hash.objects.last()
    #if hash:
        # Ensure send_hash is properly defined as an async function elsewhere
    #    blockchain_address = async_to_sync(send_hash)(hash.hash)
    #    hash.transaction_hash = blockchain_address
    #    hash.save()

    rendered_page = render(request, 'hashes/query.html', data)
    return rendered_page


def query_result(request, hash_code=None):
    """View that renders the page with the query result"""

    if hash_code:
        gen_hash = hash_code
    elif len(request.FILES) > 0:
        # Generating hash based on the provided file
        gen_hash = get_hash(request.FILES['uploaded_file'])
    elif request.POST['hash']:
        gen_hash = request.POST['hash']
    else:
        return HttpResponse('<h1>Neither a file nor a hash was provided.</h1>')

    data = check_hash_on_db(gen_hash)

    # Notice that data may be null if the hash code is not on DB.
    # Then, it should be tested in the template
    rendered_page = render(request, 'hashes/query_result.html', data)
    return rendered_page
