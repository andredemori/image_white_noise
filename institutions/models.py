from django.db import models
from django.contrib.auth.models import User


class Institution(models.Model):
    name = models.CharField(max_length=100)
    id_number = models.CharField(max_length=20, null=True, blank=True)  # like CNPJ in Brazil
    users = models.ManyToManyField(User, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name'], name='unique_name_institution')
        ]

    def __str__(self):
        return self.name
