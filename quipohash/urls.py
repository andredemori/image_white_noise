"""quipohash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from quipohash.language import set_language

from quipohash import urls

from hashes.views import upload, generate_hash, query, query_result, SuccessView, stripe_webhook, CancelView, visualizar
from hashes.view_e_certificate import e_certificate


urlpatterns = [
    # # path('', ),
    path('admin/', admin.site.urls),
    path('hashes/', include('hashes.urls')),

    path('success/', SuccessView.as_view(), name='success'),
    path('cancel/', CancelView.as_view(), name='cancel'),
    path('webhooks/stripe/', stripe_webhook, name='stripe-webhook'),


    path('hash/', generate_hash, name='hash'),
    path('query_result/', query_result, name='query_result'),
    path('query_result/<str:hash_code>/', query_result, name='query_result_with_code'),
    path('e-certificate/<int:hash_pk>/', e_certificate, name='e_certificate'),
    path('admin/', admin.site.urls),
    path('', query, name='query'),
    path('upload/', upload, name='upload'),
    path('visualizar/<str:hash>/', visualizar, name='visualizar'),
]

urlpatterns = [
    *i18n_patterns(*urlpatterns, prefix_default_language=False),
    path("set_language/<str:language>", set_language, name="set-language"),
    ]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # Adicionar Isto

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

